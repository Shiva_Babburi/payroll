package com.payroll.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.payroll.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{

	User findByUsername(String username);

}
