package com.payroll.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties(prefix = "appconfig")
@Data
public class PayrollConfigurationProperties {

	private String[] defaultUserRoles;
	private String ClientId;

	private String clientSecret;

	private String[] onPopStateUrls;
}
