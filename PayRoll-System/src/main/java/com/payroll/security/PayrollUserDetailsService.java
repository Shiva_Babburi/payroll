package com.payroll.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.payroll.entity.User;
import com.payroll.repository.UserRepository;

@Service
public class PayrollUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepo;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User optionalUser=userRepo.findByUsername(username);
		if(optionalUser != null){
			return new UserDetailsDecorator(optionalUser);
		}
		throw new UsernameNotFoundException(username);
	}

}
