package com.payroll.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.payroll.entity.User;
import com.payroll.repository.UserRepository;

@RestController
@RequestMapping("/system")
public class SystemController {

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private PasswordEncoder encoder;
	
	@GetMapping("/setup")
	public ResponseEntity<String> systemSetup(){
		String respString;
		User user=new User();
		user.setUsername("shiva");
		user.setPassword(encoder.encode("shiva"));
		user.setStatus(true);
		String[] roles= {"SYS_ADMIN"};
		ArrayList<String> al=new ArrayList<String>();
		al.add("SYS_ADMIN");
		user.setRoles(roles);
		User  createdUser=userRepo.save(user);
		if(createdUser!=null) {
			respString= "System Setup successfully";
		}
		else {
			respString="System not setup";
		}
		return new ResponseEntity<String>(respString, HttpStatus.OK);
	}
}
